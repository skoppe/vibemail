# VibeMail

Extensions for vibe's Mail class to send multi-part emails with attachments.

# Usage

The Mail and SMTP settings come from vibe. The extensions are in the `email.setContent(...)` part.

```D
import vibe.d;
import vibemail.email;

void main()
{
	runTask({
		auto settings = new SMTPClientSettings("secure.emailsrvr.com",587);
		settings.authType = SMTPAuthType.login;
		settings.connectionType = SMTPConnectionType.startTLS;
		settings.tlsValidationMode = TLSPeerValidationMode.requireCert;
		settings.username = "info@example.com";
		settings.password = "123456789";

		Mail email = new Mail;
		email.headers["Date"] = Clock.currTime().toRFC822DateTimeString();
		email.headers["Sender"] = "Domain.com Contact Form";
		email.headers["From"] = "\"Example\" <info@example.com>";
		email.headers["To"] = "\"Bas\" <bas@example.com>";
		email.headers["Subject"] = "My subject";

		// Here are the extensions
		email.setContent(
			mailMixed(
				mailRelated(
					mailAlternative(
						mailText("asdfasdfasdf"),
						mailHtml("<html><body><center>asdfasdfasdf</center></body></html>") // make sure the html comes last, else the email client won't show it by default
					),
					mailInlineImage(File("app-store.png","rb"), "image/png", "app-store.png@01D0FABA.4ECEA150", "Apple's App Store"),
				),
				mailAttachment(File("test.png","rb"),"image/png","image.png"),
				mailAttachment(cast(immutable(ubyte[]))"You are an idiot!","plain/text","text.txt")
			)
		);
		// End

		sendMail(settings, email);

		exitEventLoop();
	});

	runEventLoop();
}
```

# Notes

- `mailText` and `mailHtml` always do quoted-printable encoding
- `mailAttachment` and `mailInlineImage` always do base64 encoding
- `mailText` and `mailHtml` accept ranges, so you can also do this: `mailHtml(File("./export-mail.html").byChunk(4096).joiner)`
- `mailInlineImage` can be used to inline images inside the html. They won't show up as attachments and users don't have to click 'Show Remote Content'. In your HTML you reference them like this: `<img width="181" height="73" id="Picture_x0020_2" src="cid:app-store.png@01D0FABA.4ECEA150">`

# Todo

- Make everything fully lazy, right now there are places in the code where it eagerly converts to an array. Was probably because of some auto-decoding issues and I gave up.